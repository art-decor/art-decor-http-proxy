const axios = require('axios').default;
const logger = require('../utils/logger');
const { SCHEDULED_TASKS_POLL_INTERVAL, TARGET_BASE_URL } = require('../config');

const getNumberOfProjects = (rooms) =>
  [...rooms.entries()].filter(([key]) => key.includes('scheduled-tasks:'))
    .length;

const pollScheduledTasks = (io) => {
  setInterval(() => {
    const { rooms } = io.of('/').adapter;
    const numOfProjects = getNumberOfProjects(rooms);

    let requestCount = 0;

    logger.debug(`Polling scheduled tasks for ${numOfProjects} project(s)`);

    rooms.forEach((val, roomName) => {
      if (!roomName.includes('scheduled-tasks:')) {
        return;
      }
      const prefix = roomName.replace('scheduled-tasks:', '');
      const pollDelay = Math.round(
        (requestCount / numOfProjects) * SCHEDULED_TASKS_POLL_INTERVAL
      );
      requestCount += 1;

      logger.debug(`${val.size} client(s) listening for project ${prefix}`);

      // Make (delayed) http request and broadcast response
      setTimeout(async () => {
        try {
          const response = await axios.get(
            `${TARGET_BASE_URL}/exist/apps/api/project/${prefix}/$scheduled-task`
          );
          const { data } = response;
          io.to(roomName).emit(`GET/${prefix}/scheduled`, data);
        } catch (error) {
          logger.error(error);
        }
      }, pollDelay);
    });
  }, SCHEDULED_TASKS_POLL_INTERVAL);
};

module.exports = pollScheduledTasks;
