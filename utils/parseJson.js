const logger = require('./logger');

module.exports = (data) => {
  try {
    return JSON.parse(data);
  } catch (error) {
    logger.error(error);
    return null;
  }
};
