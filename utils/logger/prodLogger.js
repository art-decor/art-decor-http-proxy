const { createLogger, format, transports } = require('winston');
require('winston-daily-rotate-file');

const { combine, timestamp, errors, json, printf, colorize } = format;

const buildProdLogger = () => {
  const fileTransport = new transports.DailyRotateFile({
    filename: 'ad-http-proxy-%DATE%.log',
    dirname: './logs',
    datePattern: 'YYYY-MM-DD',
    maxSize: '10m',
    maxFiles: '30',
  });

  const errorConsoleFormat = printf(
    ({ level, message, timestamp, stack }) =>
      `${timestamp} ${level}: ${stack || message}`
  );

  const logger = createLogger({
    format: combine(timestamp(), errors({ stack: true }), json()),
    transports: [
      new transports.Console({
        level: 'error',
        format: combine(
          colorize(),
          timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
          errorConsoleFormat
        ),
      }),
      fileTransport,
    ],
  });

  return logger;
};

module.exports = buildProdLogger;
