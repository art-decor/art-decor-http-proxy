const express = require('express');
const { Server } = require('socket.io');
const http = require('http');

const { PORT, CORS_ORIGIN } = require('./config');
const logger = require('./utils/logger');
const httpProxy = require('./middleware/httpProxy');
const imageProxy = require('./middleware/imageProxy');
const registerScheduledTasksHandlers = require('./listeners/scheduledTasksHandlers');
const registerProjectAuthorsHandlers = require('./listeners/projectAuthorsHandlers');
const registerServerHandlers = require('./listeners/serverHandlers');
const pollScheduledTasks = require('./services/pollScheduledTasks');

// Init server
const app = express();
const server = http.createServer(app);
const io = new Server(server, {
  cors: {
    origin: CORS_ORIGIN,
  },
});

// Register listeners
const onConnection = (socket) => {
  registerScheduledTasksHandlers(io, socket);
  registerProjectAuthorsHandlers(io, socket);
  registerServerHandlers(io, socket);
};

io.on('connection', onConnection);

app.use(express.json({ strict: false, limit: '50mb' }));

// Image proxy middleware
app.use('/image-proxy', imageProxy);
// HTTP proxy middleware
app.use(httpProxy(io));

server.listen(PORT, () => {
  logger.info(`Proxy server listening on http://localhost:${PORT}`);
  pollScheduledTasks(io);
});

process.on('unhandledRejection', (error) => logger.warn(error));

process.on('uncaughtException', (error) => {
  logger.error(error);
  process.exit(1);
});
