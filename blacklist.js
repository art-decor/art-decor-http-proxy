const blacklist = ['/server/user', '/server/group', '/server-mgmt/', '/token'];

const isBlacklisted = (url) =>
  blacklist.some((urlFragment) => url.includes(urlFragment));

module.exports = { isBlacklisted };
