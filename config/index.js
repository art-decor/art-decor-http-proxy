require('dotenv').config();
const logger = require('../utils/logger');

const DEFAULTS = {
  port: 3000,
  targetBaseUrl: 'http://localhost:8877',
  corsOrigin: ['http://localhost:8080'],
  scheduledTasksPollInterval: 10000,
  logLevel: 'info',
};

const logWarning = (envVar, defaultValue) => {
  logger.warn(
    `Environment variable for ${envVar} is missing or invalid. Using default: ${defaultValue}`
  );
};

const getNumber = (envVar, defaultValue, { min, max } = {}) => {
  const envNumber = process.env[envVar];

  if (
    Number.isNaN(+envNumber) ||
    (min && +envNumber < min) ||
    (max && +envNumber > max)
  ) {
    logWarning(envVar, defaultValue);
    return defaultValue;
  }

  return +envNumber;
};

const getArrayFromCsv = (envVar, defaultValue) => {
  const envCsv = process.env[envVar];
  return (
    envCsv?.split(', ') || (logWarning(envVar, defaultValue) && defaultValue)
  );
};

const getUrl = (envVar, defaultValue) => {
  const envUrl = process.env[envVar];

  try {
    const url = new URL(envUrl);
    if (!['http:', 'https:'].includes(url.protocol)) {
      throw new Error();
    }
  } catch (error) {
    logWarning(envVar, defaultValue);
    return defaultValue;
  }
  return envUrl;
};

const getLogLevel = (defaultValue) => {
  const envLogLevel = process.env.LOG_LEVEL?.toLocaleLowerCase();
  const availableLevels = logger.levels;

  if (Object.keys(availableLevels).includes(envLogLevel)) {
    return envLogLevel;
  }

  logWarning('LOG_LEVEL', defaultValue);
  return defaultValue;
};

const setLogLevel = (level) => {
  logger.level = level;
};

const config = {
  PORT: getNumber('PORT', DEFAULTS.port),
  LOG_LEVEL: getLogLevel(DEFAULTS.logLevel),
  TARGET_BASE_URL: getUrl('TARGET_BASE_URL', DEFAULTS.targetBaseUrl),
  CORS_ORIGIN: getArrayFromCsv('CORS_ORIGIN', DEFAULTS.corsOrigin),
  SCHEDULED_TASKS_POLL_INTERVAL: getNumber(
    'SCHEDULED_TASKS_POLL_INTERVAL',
    DEFAULTS.scheduledTasksPollInterval,
    { min: 1000 }
  ),
};

setLogLevel(config.LOG_LEVEL);

module.exports = config;
