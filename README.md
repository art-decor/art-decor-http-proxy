# ART-DECOR HTTP Proxy

The **ART-DECOR HTTP Proxy** consists of two parts:

- HTTP Proxy
- WebSocket Server

Any request that is sent to the proxy server will be passed to the [ART-DECOR API](https://bitbucket.org/art-decor/art-decor-backend/src/master/). The proxy server will return the response exactly as is.

For POST, PUT, PATCH and DELETE requests, [additional request headers](#http-request-headers) can be sent to the proxy server. If those headers are present, the WebSocket Server will emit the response of the ART-DECOR API (along with some [additional information](#event-payload)) to all relevant clients.

Clients can subscribe for a [scheduled tasks](#scheduled-tasks) status update. The WebSocket Server handles the unavoidable short polling and emits the updates to all subscribers.

## Installation

**Install dependencies**

After cloning the repo install the dependencies.

```
npm install
```

**Start server in development mode**

Restarts the server on save

```
npm run dev
```

**Start server**

```
npm start
```

---

## Configuration

The following environment variables can be set in [`.env`](.env.example). There is an example `.env.example` in the repo that yopu can use as a starting point.

```
# General server config
PORT=3000
LOG_LEVEL=info

# Proxy config
TARGET_BASE_URL=http://localhost:8877

# WebSocket server config
CORS_ORIGIN=http://localhost:8080

# Polling interval for scheduled tasks (ms)
SCHEDULED_TASKS_POLL_INTERVAL=10000
```

The shown values are the default.

**Note:** To enable Cross-Origin Resource Sharing for multiple origins, specify them like `CORS_ORIGIN=http://localhost:8080, https://some-other-frontend.com`

## Usage

### Local development

- Install dependencies and start the proxy server.
- In [vue.config.js](https://bitbucket.org/art-decor/art-decor/src/master/vue.config.js), configure the devServer as follows:

```js
 devServer: {
    proxy: {
      '/api': {
        target: 'http://localhost:3000',
        changeOrigin: true,
        pathRewrite: {
          '^/api': 'exist'
        }
      }
    }
  }
```

- Start the Vue development server.

**Note:** When using a **remote API** for local Vue development, set `target` to the remote. In [.env](https://bitbucket.org/art-decor/art-decor/src/master/.env) for the Vue frontend, set `VUE_APP_SOCKET_URL=https://art-decor3.test-nictiz.nl` (or whatever the URL might be) if needed. Omitting this variable implies

### HTTP Request headers

When sending an HTTP-request to the proxy server, the following headers can be set.

- `X-Broadcast`  
  The channel used to emit the HTTP response for the initial request.  
  E.g. 'PATCH/demo1-/template'  
  Will be ignored for anything other then POST, PUT, PATCH, DELETE.  
  Only successful responses will be broadcasted.
- `X-Socket-Id`  
  Unique client identifier.

### Event payload

Listeners for a particular event recieve data in the following format:

```js
{
  request: {
    originalUrl: '/exist/apps/api/template/2.16...3.10.17/2022-04-19T16%3A54%3A54?prefix=demo1-',
    method: 'PATCH',
    emitter: 'unique-identifier-of-client-that-made-the-request'
  },
  response: {
    // the response for a HTTP request sent to the ART-DECOR API
  }
}
```

## Blacklist

The [blacklist](/blacklist.js) can be used to register API endpoints that will never cause a broadcast, even if a 'X-Broadcast' header was set on the request.

## Scheduled tasks

To subscribe for scheduled tasks updates, the client should emit an event and pass the project prefix. E.g.

```js
socket.emit('scheduled-tasks:join', { prefix: 'demo1-' });
```

It can then listen for updates on a channel called `GET/{prefix}/scheduled`.

To unsubscribe, the client should emit an event similar to the subscription.

```js
socket.emit('scheduled-tasks:leave', { prefix: 'demo1-' });
```

## Other events

Besides the scheduled tasks related events, there are some other events the WebSocket server will listen to.

| Event name               | Description                                                                                                                    | Payload                                                                                                                                                       |
| ------------------------ | ------------------------------------------------------------------------------------------------------------------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| refresh-server-info      | The WebSocket server will get the latest server info and emit it to all connected clients.                                     | N/A                                                                                                                                                           |
| refresh-server-functions | The WebSocket server will get the latest server-functions data and emit it to all connected clients.                           | N/A                                                                                                                                                           |
| projectAuthorsUpdated    | Resulting in clients fetching the project author list. By default, only for users that are an author of the specified project. | `prefix` (String, required) to specify the project. `forceApiCall` (Boolean) to also make (logged in) non-project-author users fetch the project author list. |

## Logs

Logging is based on `NODE_ENV` and `LOG_LEVEL`.

| NODE_ENV      | Description                                        |
| ------------- | -------------------------------------------------- |
| "development" | Only log to console                                |
| anything else | Log to (rotating) files <br> Log errors to console |

`LOG_LEVEL` can be set to one of the following npm logging levels:

| LOG_LEVEL | Priority |
| --------- | -------- |
| error     | 0        |
| warn      | 1        |
| info      | 2        |
| http      | 3        |
| verbose   | 4        |
| debug     | 5        |
| silly     | 6        |

> The default log level is "info". All messages with a priority of 2 or less will be logged.

## NGINX example configuration

```
# Rewrite calls to ART-DECOR API
location /exist/apps/api {
    proxy_connect_timeout       900;
    proxy_send_timeout          900;
    proxy_read_timeout          900;
    send_timeout                900;
    proxy_pass <http://localhost:3000;>
}

# Rewrite socket requests
location /socket.io {
    proxy_connect_timeout       900;
    proxy_send_timeout          900;
    proxy_read_timeout          900;
    send_timeout                900;
    proxy_pass <http://localhost:3000;>
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "upgrade";
```

Installer script for running on Linux machines can be found [here](https://bitbucket.org/art-decor/art-decor-external-tools/src/master/http-proxy-scripts/).

## Changing the default Port 3000

If under circumstanced the default port 3000 is not available, you can change it:

- Change port in `art-decor-http-proxy/.env`
- If using NGINX, the two references in the config need also to be changed to the alternative port.
