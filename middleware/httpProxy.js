const {
  createProxyMiddleware,
  responseInterceptor,
} = require('http-proxy-middleware');
const { isBlacklisted } = require('../blacklist');
const parseJson = require('../utils/parseJson');
const objectIsEmpty = require('../utils/objectIsEmpty');
const logger = require('../utils/logger');
const { TARGET_BASE_URL, LOG_LEVEL } = require('../config');

module.exports = (io) => {
  // Request interceptor
  const onProxyReq = (proxyReq, req) => {
    proxyReq.removeHeader('X-Broadcast');
    proxyReq.removeHeader('X-Socket-Id');
    req.parsedBody = req.body;

    if (!objectIsEmpty(req.body)) {
      const bodyData = JSON.stringify(req.body);
      proxyReq.setHeader('Content-Length', Buffer.byteLength(bodyData));
      proxyReq.write(bodyData);
    }
  };

  // Response interceptor
  const onProxyRes = responseInterceptor(
    async (responseBuffer, proxyRes, req, res) => {
      try {
        const ioChannel = req.get('X-broadcast');
        const isAllowedChannel =
          typeof ioChannel === 'string' &&
          !['undefined', 'false', ''].includes(ioChannel);
        const emitter = req.get('X-Socket-Id');
        const hasValidStatusCode = res.statusCode < 400;
        const reqMethod = req.method.toLowerCase();
        const methodIsAllowed = ['post', 'put', 'patch', 'delete'].includes(
          reqMethod
        );
        const hasJsonContent = res.get('Content-Type') === 'application/json';
        const hasNoContent = res.statusCode === 204;
        const broadcastAllowed = !isBlacklisted(req.originalUrl);
        const reqBody = req.parsedBody;

        if (
          ioChannel &&
          isAllowedChannel &&
          broadcastAllowed &&
          hasValidStatusCode &&
          methodIsAllowed &&
          (hasJsonContent || hasNoContent)
        ) {
          const responseString = responseBuffer.toString('utf8');
          const request = {
            originalUrl: req.originalUrl,
            method: req.method,
            emitter,
          };

          if (reqMethod === 'patch' && Array.isArray(reqBody?.parameter)) {
            request.patchPath = reqBody.parameter.map((operation) =>
              operation.path?.replace('/', '')
            );
          }

          io.emit(ioChannel, {
            request,
            response:
              responseString.length > 0 ? parseJson(responseString) : null,
          });
        }
      } catch (error) {
        logger.error(error);
      }
      return responseBuffer;
    }
  );

  const getHpmLogLevel = () => {
    const availableLevels = ['debug', 'info', 'warn', 'error', 'silent'];
    let hpmLogLevel = 'info';
    if (availableLevels.includes(LOG_LEVEL)) {
      hpmLogLevel = LOG_LEVEL;
    }
    logger.info(`HPM log level is set to "${hpmLogLevel}"`);
    return hpmLogLevel;
  };

  // Proxy configuration & initialization
  const proxyOptions = {
    target: TARGET_BASE_URL,
    selfHandleResponse: true,
    onProxyReq,
    onProxyRes,
    logLevel: getHpmLogLevel(),
    logProvider: () => logger,
  };

  return createProxyMiddleware(proxyOptions);
};
