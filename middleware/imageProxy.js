const axios = require('axios');

module.exports = (req, res) => {
  if (!req.query.url) {
    return res.send('error: no url provided');
  }

  return axios
    .get(req.query.url, { responseType: 'arraybuffer' })
    .then((response) => {
      const headers = response.headers || { ...res.headers };
      res.writeHead(200, headers);
      res.end(Buffer.from(response.data, 'binary'));
    })
    .catch((error) => {
      res.send(`error:${error}`);
    });
};
