module.exports = (io, socket) => {
  const emitProjectAuthorsUpdated = ({ prefix, forceApiCall = false } = {}) => {
    if (prefix) {
      socket.broadcast.emit(`projectAuthorsUpdated/${prefix}`, {
        forceApiCall,
      });
    }
  };

  socket.on('projectAuthorsUpdated', emitProjectAuthorsUpdated);
};
