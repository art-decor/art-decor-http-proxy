const axios = require('axios').default;
const logger = require('../utils/logger');
const { TARGET_BASE_URL } = require('../config');

module.exports = (io, socket) => {
  const getAndEmit = async ({ endpoint, eventName, callback }) => {
    let statusForCallback;

    try {
      const response = await axios.get(
        `${TARGET_BASE_URL}/exist/apps/api${endpoint}`
      );
      statusForCallback = response.status;
      const { data } = response;
      io.emit(eventName, data);
    } catch (error) {
      statusForCallback = error.response?.status || 'error';
      logger.error(error);
    } finally {
      if (typeof callback === 'function') {
        callback({ status: statusForCallback });
      }
    }
  };

  const getServerFunctions = (callback) => {
    getAndEmit({
      endpoint: '/server-mgmt/server-functions',
      eventName: 'server-functions-updated',
      callback,
    });
  };

  const getServerInfo = (callback) => {
    getAndEmit({
      endpoint: '/server',
      eventName: 'server-info-updated',
      callback,
    });
  };

  socket.on('refresh-server-info', getServerInfo);
  socket.on('refresh-server-functions', getServerFunctions);
};
