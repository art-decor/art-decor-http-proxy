module.exports = (io, socket) => {
  const joinScheduledTasksRoom = ({ prefix } = {}) => {
    if (prefix) {
      socket.join(`scheduled-tasks:${prefix}`);
    }
  };

  const leaveScheduledTasksRoom = ({ prefix } = {}) => {
    if (prefix) {
      socket.leave(`scheduled-tasks:${prefix}`);
    }
  };

  socket.on('scheduled-tasks:join', joinScheduledTasksRoom);
  socket.on('scheduled-tasks:leave', leaveScheduledTasksRoom);
};
